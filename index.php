<?php

	require 'PHPMailerAutoload.php';

	//create an instance of phpmailer
	$mail = new PHPMailer();

	//set where we are sending the email
	$mail->addAddress('giangalicinao@gmail.com', 'gian galicinao');

	//set who is sending the email
	$mail->setFrom('giangalicinao@gmail.com', 'admin');

	//set subject
	$mail->Subject = "Test Email";

	//type of email
	$mail->isHTML(true);

	//write email
	$mail->Body = "<p>Body of the email</p>
	<br>
	<br>
	<a href='www.google.com'>Google</a>
	";

	//include attachment
	// $mail->addAttachment('images/Liza S.png', 'Liza');

	//send the email
	if(!$mail->send()) {
    	echo 'Message could not be sent.';
    	echo 'Mailer Error: ' . $mail->ErrorInfo;
    }
	else {
    	echo 'Message has been sent';
	}
?>